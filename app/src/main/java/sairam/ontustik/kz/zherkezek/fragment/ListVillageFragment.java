package sairam.ontustik.kz.zherkezek.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;


import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.MyFirebaseInstanceIdService;
import sairam.ontustik.kz.zherkezek.utils.MyPreferences;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.Sender;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.MainAdapter;


public class ListVillageFragment extends Fragment {
    LinearLayoutManager layoutManager;
    VerifyConnection verifyConnection = new VerifyConnection();
    MainAdapter viewAdapter;
    RecyclerView recyclerView;
    String item;

    PlotProgressDialog progressDialog = new PlotProgressDialog();
    MyPreferences myPreferences;
    GetJsonService json = new GetJsonService();
    private BroadcastReceiver broadcastReceiver;


    public ListVillageFragment(String item) {
        this.item = item;
        System.out.println(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listvillage, container, false);
        myPreferences = new MyPreferences(getContext());
        recyclerView = (RecyclerView) view;
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(itemDecoration);

        if (verifyConnection.isNetworkAvailable(getActivity())) {
            receiveToken();
            if (myPreferences.getValue("villages") == null) {
                new VillageListAsync().execute(item);
            } else {
                villagesLocal();

            }

        } else {
            EmptyRecyclerViewAdapter emptyRecyclerViewAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.server_not_found));
            recyclerView.setAdapter(emptyRecyclerViewAdapter);
            verifyConnection.showSnackBar();
        }

        SharedPreferences preferences = getActivity().getSharedPreferences("zherkezek", Activity.MODE_PRIVATE);
        boolean receiveConfirm = preferences.getBoolean("receiveConfirm", false);
        System.out.println("ReceiveConfirm value : " + receiveConfirm);
        if (!receiveConfirm) {
            new SendTokenAndIin().execute(myPreferences.getValue("iin"), myPreferences.getValue("token"));
        }
        return view;

    }

    private void receiveToken() {
        String token = myPreferences.getValue("token");


        if (token == "") {
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    System.out.println("Check token in ListVillageFragment : " + myPreferences.getValue("token"));
                }
            };
            getActivity().registerReceiver(broadcastReceiver, new IntentFilter(MyFirebaseInstanceIdService.TOKEN_BROADCAST));
        }

        System.out.println("Check token in ListVillageFragment after : " + myPreferences.getValue("token"));

    }

    private void villagesLocal() {
        try {

            JSONArray jsonArray = new JSONArray(myPreferences.getValue("villages"));
            System.out.println("jsonArray :" + jsonArray);
            viewAdapter = new MainAdapter(jsonArray);
            recyclerView.setAdapter(viewAdapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class VillageListAsync extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            progressDialog.showDialog(getActivity(), getString(R.string.loading));
        }


        @Override
        protected Void doInBackground(String... voids) {
            System.out.println("ListVillageFragment : " + voids[0]);
            System.out.println("ListVillageFragment : " + Integer.parseInt(voids[0]));
            JSONArray list = json.getVillages(Integer.parseInt(voids[0]));


            myPreferences.save("villages", list.toString());


            viewAdapter = new MainAdapter(list);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            recyclerView.setAdapter(viewAdapter);
            progressDialog.hideDialog();
        }
    }


    private class SendTokenAndIin extends AsyncTask<String, Void, Void> {
        MyPreferences myPreferences = new MyPreferences(getContext());
        Sender sender = new Sender();

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected Void doInBackground(String... params) {
            sender.sendTokenAndIin(params[0], params[1], getContext());
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.i("zher", "Token in Async :" + myPreferences.getValue("token"));

        }
    }


}
