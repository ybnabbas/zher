package sairam.ontustik.kz.zherkezek.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;


public class PassportFragment extends Fragment {
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    JSONObject object = new JSONObject();
    VerifyConnection verifyConnection = new VerifyConnection();
    GetJsonService jsons = new GetJsonService();
    int villageId;
    TextView akim,phone,territory,population,subvillage,education,sport,medicine,economy,contact,water,safe,bonitet;

    public PassportFragment(int villageId) {
        this.villageId = villageId;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_passport,container,false);
        akim=(TextView)view.findViewById(R.id.akim);
        phone=(TextView)view.findViewById(R.id.phone);
        territory=(TextView)view.findViewById(R.id.territory);
        population=(TextView)view.findViewById(R.id.population);
        subvillage=(TextView)view.findViewById(R.id.subvillage);
        education=(TextView)view.findViewById(R.id.education);
        sport=(TextView)view.findViewById(R.id.sport);
        medicine=(TextView)view.findViewById(R.id.medicine);
        economy=(TextView)view.findViewById(R.id.economy);
        contact=(TextView)view.findViewById(R.id.contact);
        water=(TextView)view.findViewById(R.id.water);
        safe=(TextView)view.findViewById(R.id.safe);
        bonitet=(TextView)view.findViewById(R.id.bonitet);


        if (verifyConnection.isNetworkAvailable(getActivity())) {
            new PassportAsyncTask().execute(villageId);
        } else {
            verifyConnection.showSnackBar();
        }

        return view;
    }

    private class PassportAsyncTask extends AsyncTask<Integer,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),"Жүктелуде...");
        }

        @Override
        protected Void doInBackground(Integer... params) {
            object=jsons.getPassport(params[0]);
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                akim.setText(object.getString("akim"));
                bonitet.setText(object.getString("bonitet"));
                contact.setText(object.getString("contact"));
                economy.setText(object.getString("economy"));
                education.setText(object.getString("education"));
                medicine.setText(object.getString("medicine"));
                phone.setText(object.getString("phone"));
                population.setText(object.getString("population"));
                safe.setText(object.getString("safe"));
                sport.setText(object.getString("sport"));
                subvillage.setText(object.getString("subvillage"));
                territory.setText(object.getString("territory"));
                water.setText(object.getString("water"));

            }catch (JSONException e){
                e.printStackTrace();
            }

            progressDialog.hideDialog();
        }
    }
}
