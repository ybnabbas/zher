package sairam.ontustik.kz.zherkezek.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.ZherAdapter;


public class SearchFragment extends Fragment {
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    ZherAdapter viewApadter;
    EmptyRecyclerViewAdapter emptyAdapter;
    GetJsonService jsons = new GetJsonService();
   PlotProgressDialog progressDialog = new PlotProgressDialog();
    JSONArray jsonArray = new JSONArray();
    VerifyConnection verifyConnection = new VerifyConnection();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_search,container,false);

        if (verifyConnection.isNetworkAvailable(getActivity())){
            initRecyclerView(view);
            search(view.findViewById(R.id.searchView));

        }else{
            verifyConnection.showSnackBar();
        }
        return view;
    }

    private void search(SearchView searchView){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
               new SearchAsynctask().execute(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }


    private void initRecyclerView(View view) {

        recyclerView = view.findViewById(R.id.recyclerview_search);

        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);


    }

    private class SearchAsynctask extends AsyncTask<String,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),"Жүктелуде...");

        }


        @Override
        protected Void doInBackground(String... params) {
            jsonArray=jsons.search(params[0]);
            viewApadter = new ZherAdapter(jsonArray,recyclerView);

            return null;
        }



        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.hideDialog();
             if(jsonArray==null){
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.server_not_found));
                recyclerView.setAdapter(emptyAdapter);
            }
            else if (jsonArray.length()==0){
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.plot_emty));
                recyclerView.setAdapter(emptyAdapter);

            }
            else {
                recyclerView.setAdapter(viewApadter);
            }

        }
    }
}
