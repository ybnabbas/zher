package sairam.ontustik.kz.zherkezek.utils.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.PopupActivity;
import sairam.ontustik.kz.zherkezek.R;

public class ListSubVillagesRecyclerViewAdapter extends RecyclerView.Adapter<ListSubVillagesRecyclerViewAdapter.ViewHolder> {

    private JSONArray list;

    public ListSubVillagesRecyclerViewAdapter(){}

    public ListSubVillagesRecyclerViewAdapter(JSONArray list){
        this.list = list;
    }

    @Override
    public ListSubVillagesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_subvillages_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ListSubVillagesRecyclerViewAdapter.ViewHolder holder, int position) {
        try {
            JSONObject jsonObject=(JSONObject) list.get(position);
            holder.list_subvillages.setText(jsonObject.getString("name"));
            holder.subvillage_id.setText(jsonObject.getString("id"));
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return list.length();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView list_subvillages,subvillage_id;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            list_subvillages = (TextView) view.findViewById(R.id.list_subvillages);
            subvillage_id=(TextView) view.findViewById(R.id.subvillage_id);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), PopupActivity.class);
            intent.putExtra("item",3);
            intent.putExtra("subvillage_id",subvillage_id.getText());
            Log.i("zherIlyas",subvillage_id.getText().toString());
            intent.putExtra("village",list_subvillages.getText());
            v.getContext().startActivity(intent);

        }
    }
}