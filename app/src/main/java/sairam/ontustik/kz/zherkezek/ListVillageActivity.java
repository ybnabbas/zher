package sairam.ontustik.kz.zherkezek;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import sairam.ontustik.kz.zherkezek.fragment.ListVillageFragment;
import sairam.ontustik.kz.zherkezek.fragment.VillageFragment;
import sairam.ontustik.kz.zherkezek.utils.MyPreferences;

import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP;

public class ListVillageActivity extends BaseActivity {


private static int ACTIVITY_VILLAGE =R.layout.activity_village;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.SearchTheme);
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout=(FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(ACTIVITY_VILLAGE,frameLayout);
        AppBarLayout.LayoutParams params =(AppBarLayout.LayoutParams)toolbar.getLayoutParams();
        params.setScrollFlags(SCROLL_FLAG_SCROLL |SCROLL_FLAG_ENTER_ALWAYS |SCROLL_FLAG_SNAP);

        initInfo();

    }

    @Override
    public void onBackPressed() {
       finishAffinity();

    }

    private void initInfo() {
        MyPreferences preferences =new MyPreferences(this);

        String itemId = getIntent().getStringExtra("itemId");
        System.out.println("ListVillageactivity : " +itemId);
        toolbar.setTitle(preferences.getValue("title"));

        Fragment contextFragment= new ListVillageFragment(itemId);
        fragmentManager.beginTransaction().replace(R.id.content_frame,contextFragment).commit();


    }
}
