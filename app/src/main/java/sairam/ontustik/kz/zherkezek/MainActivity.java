package sairam.ontustik.kz.zherkezek;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import sairam.ontustik.kz.zherkezek.fragment.pager.IinFragment;


public class MainActivity extends BaseActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.DefaultTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        SharedPreferences sharedPreferences=getSharedPreferences("zherkezek", Activity.MODE_PRIVATE);
     String districtId=sharedPreferences.getString("districtId",null);
        System.out.println("districtId in SharedPreferences : " +districtId);

        if (districtId!=null){

            initFragment(districtId);

        }else {
            initIinFragment();
        }


    }



    private void initIinFragment(){
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container,new IinFragment());
        fragmentTransaction.commit();

    }



  private void initFragment(String districtId){
      System.out.println();
      Intent listPlot = new Intent(this, ListVillageActivity.class);

      listPlot.putExtra("itemId", districtId);
      this.startActivity(listPlot);

  }





}
