package sairam.ontustik.kz.zherkezek;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import sairam.ontustik.kz.zherkezek.fragment.PersonalFragment;

import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP;

public class PersonalActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout=(FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_personal,frameLayout);
        AppBarLayout.LayoutParams params =(AppBarLayout.LayoutParams)toolbar.getLayoutParams();
        params.setScrollFlags(SCROLL_FLAG_SCROLL |SCROLL_FLAG_ENTER_ALWAYS |SCROLL_FLAG_SNAP);
        toolbar.setTitle(getResources().getString(R.string.personal_card));
           initFragment();
    }


    private void initFragment(){
        int id=getIntent().getIntExtra("id",0);
        Fragment contextFragment= new PersonalFragment(id);
        fragmentManager.beginTransaction().replace(R.id.content_frame,contextFragment).commit();
    }




}
