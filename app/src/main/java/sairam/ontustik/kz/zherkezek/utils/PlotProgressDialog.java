package sairam.ontustik.kz.zherkezek.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.ProgressBar;

import sairam.ontustik.kz.zherkezek.R;


public class PlotProgressDialog {
    ProgressDialog progressDialog;

    public void showDialog(Activity activity,String message){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(false);
        progressDialog.show();
    }

    public void hideDialog(){
        progressDialog.dismiss();
    }

    public void showLoader(ProgressBar loader){
       loader.setVisibility(View.VISIBLE);
    }

    public void hideLoader(ProgressBar loader){
        loader.setVisibility(View.GONE);
    }


}

