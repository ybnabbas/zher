package sairam.ontustik.kz.zherkezek.utils;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;


public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    public static final String TOKEN_BROADCAST="myfcmtokenbroadcast";

    @Override
    public void onTokenRefresh() {

        String token= FirebaseInstanceId.getInstance().getToken();
        MyPreferences myPreferences = new MyPreferences(getApplicationContext());
        myPreferences.save("token",token);
        getApplicationContext().sendBroadcast(new Intent(TOKEN_BROADCAST));

        System.out.println("Refresh token here : " +token);
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        FirebaseMessaging.getInstance().subscribeToTopic("auction");

    }

}
