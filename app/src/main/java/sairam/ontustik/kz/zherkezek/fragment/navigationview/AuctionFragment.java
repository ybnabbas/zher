package sairam.ontustik.kz.zherkezek.fragment.navigationview;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.NfAdapter;


public class AuctionFragment extends Fragment {
    VerifyConnection verifyConnection = new VerifyConnection();
    LinearLayoutManager layoutManager;
    JSONArray jsonArray;
    NfAdapter viewApadter;
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    EmptyRecyclerViewAdapter emptyAdapter;
    RecyclerView recyclerView;
    GetJsonService jsons = new GetJsonService();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_auction,container,false);
        if (verifyConnection.isNetworkAvailable(getActivity())){
            initrecyclerView(view.findViewById(R.id.recyclerview_fragment_auction));
            new NotificationAsyncTask().execute();
        }else{
            verifyConnection.showSnackBar();
        }
        return view;
    }
    private void initrecyclerView(View view) {
        recyclerView = (RecyclerView) view;
        layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);



    }
    private class NotificationAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),getString(R.string.loading));
        }

        @Override
        protected Void doInBackground(Void... params) {

            jsonArray = jsons.getAuctions();
            viewApadter = new NfAdapter(jsonArray,"2");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(jsonArray==null){
                emptyAdapter = new EmptyRecyclerViewAdapter(getString(R.string.server_not_found));
                recyclerView.setAdapter(emptyAdapter);
            }
            else if (jsonArray.length()==0){
                emptyAdapter = new EmptyRecyclerViewAdapter(getString(R.string.not_auction_now));
                recyclerView.setAdapter(emptyAdapter);

            }
            else {
                recyclerView.setAdapter(viewApadter);
            }
            progressDialog.hideDialog();
        }
    }
}
