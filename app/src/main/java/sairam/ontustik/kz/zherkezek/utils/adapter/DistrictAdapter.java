package sairam.ontustik.kz.zherkezek.utils.adapter;


import android.app.Activity;
import android.content.Intent;

import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;



import sairam.ontustik.kz.zherkezek.ListVillageActivity;
import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.MyPreferences;


public class DistrictAdapter extends RecyclerView.Adapter {
    JSONArray list;


    public DistrictAdapter(JSONArray list) {
        this.list = list;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_district, parent, false);
        RecyclerView.ViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            MyViewHolder myViewHolder = (MyViewHolder) holder;
            System.out.println("Check list : " +list.getJSONObject(position));
            myViewHolder.districtName.setText(list.getJSONObject(position).getString("name"));

            myViewHolder.districtId.setText(list.getJSONObject(position).getInt("id")+"");
            char firstChar = myViewHolder.districtName.getText().charAt(0);
            myViewHolder.asd.setText(firstChar + "");


            switch (firstChar) {
                case 'А':
                    myViewHolder.asd.setBackground(myViewHolder.districtName.getResources().getDrawable(R.drawable.bg_main_sary));
                    break;
                case 'Қ':
                    myViewHolder.asd.setBackground(myViewHolder.districtName.getResources().getDrawable(R.drawable.bg_main_kok));
                    break;
                case 'Ж':
                    myViewHolder.asd.setBackground(myViewHolder.districtName.getResources().getDrawable(R.drawable.bg_main_qyzyl));
                    break;
                case 'М':
                    myViewHolder.asd.setBackground(myViewHolder.districtName.getResources().getDrawable(R.drawable.bg_main_m));
                    break;
                case 'К':
                    myViewHolder.asd.setBackground(myViewHolder.districtName.getResources().getDrawable(R.drawable.bg_main_zhasyl));
                    break;
                default:
                    myViewHolder.asd.setBackground(myViewHolder.districtName.getResources().getDrawable(R.drawable.bg_main_sary));
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        try {
            return list.length();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 0;
        }

    }


    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView districtName;
        TextView asd;
        TextView districtId;

        public MyViewHolder(View itemView) {
            super(itemView);
            districtName =  itemView.findViewById(R.id.districtName);
            districtId =  itemView.findViewById(R.id.districtId);
            asd =  itemView.findViewById(R.id.asd);

            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {

            MyPreferences preferences=new MyPreferences(v.getContext());
            preferences.save("districtId",districtId.getText().toString());
            preferences.save("title",districtName.getText().toString());

            Intent listPlot = new Intent(v.getContext(), ListVillageActivity.class);
            listPlot.putExtra("itemId", districtId.getText());
            v.getContext().startActivity(listPlot);

        }



    }


}