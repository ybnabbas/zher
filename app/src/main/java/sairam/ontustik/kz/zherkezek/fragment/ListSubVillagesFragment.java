package sairam.ontustik.kz.zherkezek.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.ListSubVillagesRecyclerViewAdapter;


public class ListSubVillagesFragment extends Fragment {
    int id;
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    LinearLayoutManager layoutManager;
    JSONArray jsonArray = new JSONArray();
    VerifyConnection verifyConnection = new VerifyConnection();
    ListSubVillagesRecyclerViewAdapter viewAdapter;
    GetJsonService jsons = new GetJsonService();
    EmptyRecyclerViewAdapter emptyAdapter;
    RecyclerView rv_list_subvillages;


    public ListSubVillagesFragment(int id) {
        this.id = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_pdp, container, false);
        if (verifyConnection.isNetworkAvailable(getActivity())) {
            initrecyclerView(view.findViewById(R.id.rv_list_subvillages));
        } else {
            verifyConnection.showSnackBar();
        }
        return view;
    }

    private void initrecyclerView(View view) {
        this.rv_list_subvillages = (RecyclerView) view;
        layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(rv_list_subvillages.getContext(), layoutManager.getOrientation());
        rv_list_subvillages.setLayoutManager(layoutManager);
        rv_list_subvillages.addItemDecoration(itemDecoration);
        new ListSubVillagesAsyck().execute(id);
    }


    private class ListSubVillagesAsyck extends AsyncTask<Integer, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(), "Жүктелуде...");

        }

        @Override
        protected Void doInBackground(Integer... params) {
            jsonArray = jsons.getListSubVillages(params[0]);

            viewAdapter = new ListSubVillagesRecyclerViewAdapter(jsonArray);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (jsonArray == null) {
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.server_not_found));
                rv_list_subvillages.setAdapter(emptyAdapter);
            } else {
                if (jsonArray.length() == 0) {
                    emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.now_info_not));
                    rv_list_subvillages.setAdapter(emptyAdapter);

                } else {
                    rv_list_subvillages.setAdapter(viewAdapter);
                }

            }
            progressDialog.hideDialog();
        }
    }
}
