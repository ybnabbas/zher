package sairam.ontustik.kz.zherkezek.utils.nf;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import sairam.ontustik.kz.zherkezek.AdvertActivity;
import sairam.ontustik.kz.zherkezek.R;


public class NotificationBuilder extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String body = remoteMessage.getNotification().getBody();
        String title = remoteMessage.getNotification().getTitle();
        String type = remoteMessage.getData().get("type");
        String id = remoteMessage.getData().get("id");
        showNotification(body, title, type, id);
        Log.i("zher", "Message_id : " + id);
        Log.i("zher", "Click Action : " + remoteMessage.getNotification().getClickAction());
    }

    public void showNotification(String messageBody, String messageTitle,String type,String id) {

        String decoderBody = null;
        String decoderTitle = null;
        try {
            decoderBody = URLDecoder.decode(messageBody, "UTF-8");
            decoderTitle = URLDecoder.decode(messageTitle, "UTF-8");
            Log.i("zher", "decodeBody : " + decoderBody);
            Log.i("zher", "decoderTitle : " + decoderTitle);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)

                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker(this.getResources().getString(R.string.sponsor))
                .setWhen(System.currentTimeMillis())
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentTitle(decoderTitle)
                .setContentText(decoderBody)
                .setAutoCancel(true);
//------------------------------------------------------------------------------//
Log.i("zher", id);
        Log.i("zher", type);
        Intent intent = new Intent(this, AdvertActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("id", id);
        intent.putExtra("type", type);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, Integer.parseInt(id), intent, PendingIntent.FLAG_CANCEL_CURRENT);
        builder.setContentIntent(pendingIntent);
        //

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(12, builder.build());

        //Activity


    }
}
