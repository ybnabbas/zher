package sairam.ontustik.kz.zherkezek.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;


public class PersonalFragment extends Fragment {
    VerifyConnection verifyConnection = new VerifyConnection();
    int id;
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    TextView fr_id, fr_iin, fr_fullName, fr_queue, fr_filed, fr_completion, fr_status, fr_village;
    GetJsonService jsons = new GetJsonService();
    JSONObject object = new JSONObject();

    public PersonalFragment() {
    }

    public PersonalFragment(int id) {
        this.id = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_personal, container, false);

        fr_completion = view.findViewById(R.id.fr_completion);
        fr_id = view.findViewById(R.id.fr_id);
        fr_iin = view.findViewById(R.id.fr_iin);
        fr_fullName = view.findViewById(R.id.fr_fullname);
        fr_queue = view.findViewById(R.id.fr_queue);
        fr_filed = view.findViewById(R.id.fr_filed);
        fr_queue = view.findViewById(R.id.fr_queue);
        fr_status = view.findViewById(R.id.fr_status);
        fr_village = view.findViewById(R.id.fr_village);



        if (verifyConnection.isNetworkAvailable(getActivity())) {
            new PersonalAsyncTask().execute(id);
        } else {
            verifyConnection.showSnackBar();
        }
        return view;
    }


    private void display() {
        SharedPreferences sharedPref = getActivity().getSharedPreferences("info", Context.MODE_PRIVATE);
        String id = sharedPref.getString("id", "");
    }

    private class PersonalAsyncTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(), "Жүктелуде...");
        }

        @Override
        protected Void doInBackground(Integer... params) {
            object = jsons.getPlot(params[0]);
            System.out.println(object);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                switch (object.getInt("status")) {
                    case 1:
                        fr_status.setText(R.string.tapsirsildi);
                        fr_status.setBackgroundResource(R.drawable.martebe_kutu);
                        break;
                    case 2:
                        fr_status.setText(R.string.queue);
                        fr_status.setBackgroundResource(R.drawable.martebe_kutu);
                        break;
                    case 3:
                        fr_status.setText(R.string.refuse);
                        fr_status.setBackgroundResource(R.drawable.martebe_otkaz);
                        break;
                    case 4:
                        fr_status.setText(R.string.get);
                        fr_status.setBackgroundResource(R.drawable.martebe_berildi);
                        break;
                    default:
                        fr_status.setText(R.string.unknown);
                        fr_status.setBackgroundResource(R.drawable.martebe_otkaz);
                }
                fr_village.setText(object.getString("villageName"));

                fr_queue.setText(object.getString("queue"));
                fr_filed.setText(object.getString("applyDate"));
                fr_queue.setText(object.getString("queue"));
                fr_fullName.setText(object.getString("fullName"));
                fr_iin.setText(object.getString("iin"));
                fr_id.setText(object.getString("id"));
                fr_completion.setText(object.getString("completion"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            progressDialog.hideDialog();
        }
    }


}
