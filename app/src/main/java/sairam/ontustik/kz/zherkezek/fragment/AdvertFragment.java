package sairam.ontustik.kz.zherkezek.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;


public class AdvertFragment extends Fragment {
    VerifyConnection verifyConnection = new VerifyConnection();
    String id;
    String type;
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    TextView content, title;
GetJsonService jsons = new GetJsonService();
    JSONObject object = new JSONObject();
    public AdvertFragment() {
    }

    public AdvertFragment(String id,String type) {
        this.id = id;
        this.type=type;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_advert, container, false);
        title=view.findViewById(R.id.advert_title);
        content =view.findViewById(R.id.advert_content);

        if (verifyConnection.isNetworkAvailable(getActivity())) {
            switch (type){
                case "1":
                    new NewsAsyncTask().execute(id);
                    break;
                case "2":
                    new AuctionAsyncTask().execute(id);
                    break;
                case "3":
                    new NfAsyncTask().execute(id);
                    break;
                case "4":
                    new AnswerAsyncTask().execute(id);
                    break;
                default:
                    new NewsAsyncTask().execute(id);
                break;
            }

        } else {
            verifyConnection.showSnackBar();
        }
        return view;
    }



    private class NfAsyncTask extends AsyncTask<String,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
              progressDialog.showDialog(getActivity(),getResources().getString(R.string.loading));
        }
        @Override
        protected Void doInBackground(String... params) {
            object=jsons.getNf(params[0]);
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                title.setText(object.getString("title"));
                content.setText(object.getString("content"));



            } catch (JSONException e) {
                e.printStackTrace();
            }
              progressDialog.hideDialog();
        }
    }
    private class NewsAsyncTask extends AsyncTask<String,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),"Жүктелуде...");
        }
        @Override
        protected Void doInBackground(String... params) {
            object=jsons.getNew(params[0]);
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                title.setText(object.getString("text"));
                content.setText(object.getString("content"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressDialog.hideDialog();
        }
    }

    private class AnswerAsyncTask extends AsyncTask<String,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),"Жүктелуде...");
        }

        @Override
        protected Void doInBackground(String... params) {
            object=jsons.getAnswer(Integer.parseInt(params[0]));
            System.out.println(object);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                title.setText("Сұрақ : "+object.getString("title"));
                content.setText("Жауап : "+object.getString("answer"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressDialog.hideDialog();
        }


    }
    private class AuctionAsyncTask extends AsyncTask<String,Void,Void>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),"Жүктелуде...");
        }
        @Override
        protected Void doInBackground(String... params) {
            object=jsons.getAuction(params[0]);
            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                title.setText(object.getString("title"));
                content.setText(object.getString("content"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            progressDialog.hideDialog();
        }
    }
}
