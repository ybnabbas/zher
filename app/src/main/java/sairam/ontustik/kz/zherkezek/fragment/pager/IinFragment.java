package sairam.ontustik.kz.zherkezek.fragment.pager;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.MyPreferences;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;

public class IinFragment extends Fragment {
VerifyConnection connection= new VerifyConnection();
    EditText iinEditor;
    Button secondPage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view=inflater.inflate(R.layout.fragment_iin,container,false);
        iinEditor=view.findViewById(R.id.iinEditor);
        secondPage =(Button) view.findViewById(R.id.secondPage);

        iinEditor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()==12){
                    secondPage.setVisibility(View.VISIBLE);
                }else {
                    secondPage.setVisibility(View.INVISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

secondPage.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (connection.isNetworkAvailable(getActivity())){
            new SaveIin().execute(iinEditor.getText().toString());
            FragmentTransaction transaction=getFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
            transaction.replace(R.id.fragment_container, new DistrictFragment());
            transaction.commit();
        }else {
            iinEditor.setVisibility(View.INVISIBLE);
            connection.showSnackBar();
        }



    }
});



        return view;
    }

    private class SaveIin extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... voids) {

            MyPreferences preferences = new MyPreferences(getContext());

            preferences.save("iin",voids[0]);

            return null;
        }
    }


}
