package sairam.ontustik.kz.zherkezek;


import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import sairam.ontustik.kz.zherkezek.fragment.SearchFragment;
import sairam.ontustik.kz.zherkezek.utils.InitialNavigationVIew;

public class BaseActivity extends AppCompatActivity {
    private static final int LAYOUT_MAIN = R.layout.activity_base;

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    NavigationView navigationView;
    InitialNavigationVIew navigationVIew = new InitialNavigationVIew();
    FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.DefaultTheme);
        super.onCreate(savedInstanceState);
        setContentView(LAYOUT_MAIN);
        initToolbar();
        initNavigationView();

    }





    private void initNavigationView() {
        navigationView = (NavigationView) findViewById(R.id.nav_menu_main);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout_main);
        navigationVIew.initilize(this, drawerLayout, navigationView, toolbar);

    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        toolbar.setTitle(R.string.zher_kommisiya);
        toolbar.inflateMenu(R.menu.menu_main);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorWhite));
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                toolbar.setTitle(item.getTitle());
                switch (item.getItemId()) {
                    case R.id.search:
                        Fragment news = new SearchFragment();
                        fragmentManager.beginTransaction().replace(R.id.content_frame, news).commit();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }


}
