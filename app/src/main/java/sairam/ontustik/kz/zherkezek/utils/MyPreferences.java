package sairam.ontustik.kz.zherkezek.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Map;


public class MyPreferences {
    Context context;


    public MyPreferences(Context context) {
        this.context = context;
    }


    public void save(String key, String value) {
        SharedPreferences sharedPref = context.getSharedPreferences("zherkezek", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);

        editor.commit();
    }

    public String getValue(String key) {
        SharedPreferences sharedPref = context.getSharedPreferences("zherkezek", Activity.MODE_PRIVATE);
        return sharedPref.getString(key, null);
    }


    public Map<String,?> getAll(){
        return PreferenceManager.getDefaultSharedPreferences(context).getAll();
    }
}
