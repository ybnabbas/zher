package sairam.ontustik.kz.zherkezek.utils.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import sairam.ontustik.kz.zherkezek.fragment.pager.*;


public class IinAdapter extends FragmentPagerAdapter {


    public IinAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new IinFragment();
            case 1:
                return new DistrictFragment();

        }
        return null; //орындалмайды

    }

    @Override
    public int getCount() {
        return 2;
    }
}
