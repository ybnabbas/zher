package sairam.ontustik.kz.zherkezek.fragment.pager;


import android.app.Activity;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.adapter.DistrictAdapter;

public class DistrictFragment extends Fragment {
    LinearLayoutManager layoutManager;
    RecyclerView.Adapter viewAdapter;
    RecyclerView recyclerView;
    GetJsonService jsons = new GetJsonService();
    PlotProgressDialog progressDialog = new PlotProgressDialog();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_district, container, false);
        Button previousBtn = view.findViewById(R.id.previousBtn);
        recyclerViewer(view);

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("zherkezek", Activity.MODE_PRIVATE);
        System.out.println(sharedPreferences.getString("iin", "ИИН жоқ"));


        previousBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                transaction.replace(R.id.fragment_container, new IinFragment());
                transaction.commit();
            }
        });
        return view;
    }


    private void recyclerViewer(View view) {
        recyclerView = view.findViewById(R.id.recyclerDistricts);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.addItemDecoration(itemDecoration);
        new DistrictAsync().execute();

    }


    private class DistrictAsync extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.showDialog(getActivity(), getString(R.string.loading));
        }

        @Override
        protected Void doInBackground(Void... voids) {
            JSONArray jsonArray = jsons.getDistricts();
            viewAdapter = new DistrictAdapter(jsonArray);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            recyclerView.setAdapter(viewAdapter);
            progressDialog.hideDialog();
        }
    }


}
