package sairam.ontustik.kz.zherkezek.fragment.navigationview;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.fragment.notification.NotificationAnswerFragment;
import sairam.ontustik.kz.zherkezek.fragment.notification.NotificationQueueFragment;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;
import sairam.ontustik.kz.zherkezek.utils.adapter.ViewPagerAdapter;


public class NotificationFragment extends Fragment {
    VerifyConnection verifyConnection = new VerifyConnection();
    TabLayout tabLayout;
    ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.fragment_notification,container,false);

        if (verifyConnection.isNetworkAvailable(getActivity())){
         initTab(view);
        }else{
            verifyConnection.showSnackBar();
        }
        return view;
    }

private void initTab(View view){
    tabLayout=(TabLayout) view.findViewById(R.id.tablayout_notification);
    viewPager=(ViewPager) view.findViewById(R.id.viewpager_notification);
    setupViewPager(viewPager);
    tabLayout.setupWithViewPager(viewPager);



}

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPagerAdapter.addFragment(new NotificationQueueFragment(), getResources().getString(R.string.queue_tab));
        viewPagerAdapter.addFragment(new NotificationAnswerFragment(), getResources().getString(R.string.asnwer_tab));

        viewPager.setAdapter(viewPagerAdapter);

    }




}
