package sairam.ontustik.kz.zherkezek.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;

import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;


public class DetDesProFragment extends Fragment {
    int id;
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    JSONObject object = new JSONObject();
    VerifyConnection verifyConnection = new VerifyConnection();
    GetJsonService jsons = new GetJsonService();
    String base64;
    PhotoView imagePdp;
    public DetDesProFragment(int id) {
        this.id = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_detdespro,container,false);
        imagePdp=(PhotoView) view.findViewById(R.id.imagePdp);
        imagePdp.setMaximumScale(10);
        if (verifyConnection.isNetworkAvailable(getActivity())) {
            new PdpImage().execute(id);
        } else {
            verifyConnection.showSnackBar();
        }
        return view;
    }


    private class PdpImage extends AsyncTask<Integer,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),"Жүктелуде...");
        }

        @Override
        protected Void doInBackground(Integer... params) {
            object=jsons.getPdp(params[0]);
            try {
                base64=object.getString("pdp");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            imagePdp.setImageBitmap(decodedByte);


            progressDialog.hideDialog();
        }
    }
}
