package sairam.ontustik.kz.zherkezek.fragment.navigationview;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.MainActivity;
import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.MyPreferences;
import sairam.ontustik.kz.zherkezek.utils.MyResourses;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.ZherAdapter;


public class DeleteFragment extends Fragment {





    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_queue_plot, container, false);
        showHelpWindow();
        return view;

    }

    private void deleteCard(){
        SharedPreferences preferences =getContext().getSharedPreferences("zherkezek", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
        Intent intent= new Intent(getContext(), MainActivity.class);
        getContext().startActivity(intent);
    }
    public void showHelpWindow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(MyResourses.getString(getContext(), R.string.question_title)).setItems(R.array.deleteCard, null).setNegativeButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteCard();
            }
        }).setPositiveButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent= new Intent(getActivity(), MainActivity.class);
                getActivity().startActivity(intent);
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }



}
