package sairam.ontustik.kz.zherkezek.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;



public class GetJsonService {
  HttpURLConnection connection;
    URL url;
    JSONBuilderService jsonBuilderService = new JSONBuilderService();
//http://78.40.108.89:8080/ZherKezekServer/sairam/plot/
public  final String server="http://185.22.66.48/";

    //Метод для списка обьекта из конкретного village
    public JSONArray getPlots(int role,int district,int village, int status, int start,int finish){

        return jsonBuilderService.createArray(server+"plot/getList?role="+role+"&district="+district+"&village="+village+"&status="+status+"&start="+start+"&finish="+finish);
    }
    //Метод для получение конкретного обьекта
    public JSONObject getPlot(int id){
        return jsonBuilderService.createObject(server+"plot/get?id="+id);
    }

    //Метод для получение Паспорт обьекта
    public JSONObject getPassport(int id){
        return jsonBuilderService.createObject(server+"passport/"+id);
    }
    //Метод для получение Паспорт обьекта
    public JSONArray getListSubVillages(int id){
        return jsonBuilderService.createArray(server+"pdp/"+id);
    }

    //Метод для получение список округа по району
    public JSONArray getVillages(int district){
        return jsonBuilderService.createArray(server+"village/list?district="+district);
    }

    //Метод для получение Паспорт обьекта
    public JSONObject getPdp(int id){
        return jsonBuilderService.createObject(server+"pdpsingle/"+id);
    }



    //Метод для списка уведомление
    public JSONArray getNfs(String iin){
        return jsonBuilderService.createArray(server+"notification/getList?iin="+iin);
    }
    //Метод для получение конкретного уведомление
    public JSONObject getNf(String id){
        return jsonBuilderService.createObject(server+"notification/get?id="+id);
    }

    //Метод для списка ответа
    public JSONArray getAnswers(String iin){
        return jsonBuilderService.createArray(server+"question/getListWithIin?iin="+iin);
    }
    //Метод для списка Новости
    public JSONArray getNews(int start,int finish){
        return jsonBuilderService.createArray(server+"news/list?start="+start+"&finish="+finish+"");
    }
    //Метод для получение конкретного новости
    public JSONObject getNew(String id){
        return jsonBuilderService.createObject(server+"news/get?id="+id);
    }
    //Метод для списка Аукцион
    public JSONArray getAuctions(){
        return jsonBuilderService.createArray(server+"auction/get");
    }
    //Метод для списка Района
    public JSONArray getDistricts(){
        return jsonBuilderService.createArray(server+"districts");
    }



    //Метод для получение конкретного Аукциона
    public JSONObject getAuction(String id){
        return jsonBuilderService.createObject(server+"auctions/"+id);
    }
    //Метод для счетчика уведомление
    public JSONObject getNfUnReadCount(int user){
        return jsonBuilderService.createObject(server+"notifications/count/"+user);
    }



    //Метод  для посика
    public JSONArray search(String value){
        String param = null;
        try {
             param= URLEncoder.encode(value,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return jsonBuilderService.createArray(server+"plot/find?key="+param+"");

    }

    public JSONObject getAnswer(int id) {
        return jsonBuilderService.createObject(server+"/question/getId?id="+id);
    }


}
