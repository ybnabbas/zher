package sairam.ontustik.kz.zherkezek.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.View;

import sairam.ontustik.kz.zherkezek.R;

/**
 * Created by Ilyas on 12.06.2017.
 */

public class VerifyConnection {
    Activity context;
    CoordinatorLayout coordinatorLayout;
    public boolean isNetworkAvailable(Activity context)
    {
        this.context=context;
        coordinatorLayout=(CoordinatorLayout) context.findViewById(R.id.coordinatorLayout);
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void showSnackBar(){
       Snackbar snackbar=Snackbar.make(context.findViewById(android.R.id.content), R.string.inetoff,Snackbar.LENGTH_INDEFINITE).setAction(R.string.inetupdate, new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               context.finish();
               context.startActivity(context.getIntent());
           }
       });
        snackbar.show();
    }
}
