package sairam.ontustik.kz.zherkezek.fragment.plot;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.MyPreferences;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.ZherAdapter;


public class GetFragment extends Fragment {
    LinearLayoutManager layoutManager;
    ZherAdapter viewApadter;
    GetJsonService jsons = new GetJsonService();
    RecyclerView recyclerView;
    JSONArray jsonArray = new JSONArray();
    EmptyRecyclerViewAdapter emptyAdapter;
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    int village;
    ProgressBar progressBar;
    int finishCard = 50;

    public GetFragment(int village) {
        this.village = village;
    }

    public GetFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_get_plot, container, false);
        initrecyclerView(view);
        return view;

    }

    private void initrecyclerView(View view) {
        this.recyclerView = view.findViewById(R.id.recyclerview_get_plot);
        progressBar=view.findViewById(R.id.loader_get);
        layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);

        new GetAsyncTask().execute(finishCard);


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {

                    finishCard=finishCard+50;
                    new GetAsyncTask().execute(finishCard);


                }

            }


        });
    }

    private class GetAsyncTask extends AsyncTask<Integer, Void, Void> {


        @Override
        protected void onPreExecute() {
            progressDialog.showLoader(progressBar);

        }

        @Override
        protected Void doInBackground(Integer... params) {

            jsonArray = jsons.getPlots(0, 0, village, 3, 0, params[0]);
            viewApadter = new ZherAdapter(jsonArray, recyclerView);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            //   progressDialog.hideDialog();
            if (jsonArray == null) {
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.server_not_found));
                recyclerView.setAdapter(emptyAdapter);
            } else if (jsonArray.length() == 0) {
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.plot_emty));
                recyclerView.setAdapter(emptyAdapter);

            } else {
                recyclerView.setAdapter(viewApadter);
            }
            progressDialog.hideLoader(progressBar);

            recyclerView.getLayoutManager().scrollToPosition(finishCard-50);
        }


    }
}
