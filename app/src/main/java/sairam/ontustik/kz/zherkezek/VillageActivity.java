package sairam.ontustik.kz.zherkezek;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import sairam.ontustik.kz.zherkezek.fragment.VillageFragment;

import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP;

public class VillageActivity extends BaseActivity {


private static int ACTIVITY_VILLAGE =R.layout.activity_village;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.SearchTheme);
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout=(FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(ACTIVITY_VILLAGE,frameLayout);
        AppBarLayout.LayoutParams params =(AppBarLayout.LayoutParams)toolbar.getLayoutParams();
        params.setScrollFlags(SCROLL_FLAG_SCROLL |SCROLL_FLAG_ENTER_ALWAYS |SCROLL_FLAG_SNAP);

        initInfo();

    }



    private void initInfo() {
        int itemId = getIntent().getIntExtra("itemId", 1);
        toolbar.setTitle(getIntent().getStringExtra("name"));

        Fragment contextFragment= new VillageFragment(itemId);
        fragmentManager.beginTransaction().replace(R.id.content_frame,contextFragment).commit();


    }
}
