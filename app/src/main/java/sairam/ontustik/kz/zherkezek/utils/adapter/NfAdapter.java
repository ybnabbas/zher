package sairam.ontustik.kz.zherkezek.utils.adapter;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.AdvertActivity;
import sairam.ontustik.kz.zherkezek.R;


public class NfAdapter extends RecyclerView.Adapter {
    JSONArray list;
    String type;

    public NfAdapter(JSONArray list,String type) {
        this.list = list;
        this.type=type;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_advert, parent, false);
        RecyclerView.ViewHolder holder = new NewsViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            NewsViewHolder myViewHolder = (NewsViewHolder) holder;
            JSONObject jsonObject = (JSONObject) list.get(position);
            myViewHolder.date.setText(jsonObject.getString("date"));
            myViewHolder.title.setText(jsonObject.getString("title"));
            myViewHolder.id.setText(jsonObject.getString("id"));
            switch (jsonObject.getInt("status_id")) {
                case 1:
                    myViewHolder.title.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD);
                    break;
                case 2:
                    myViewHolder.title.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
                    break;
                default:
                    myViewHolder.title.setTypeface(Typeface.SANS_SERIF, Typeface.NORMAL);
            }

        } catch (JSONException e) {
            e.getMessage();
        }
    }


    @Override
    public int getItemCount() {
        try {
            return list.length();
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 0;
        }

    }

    class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView date, title, id;

        public NewsViewHolder(View itemView) {
            super(itemView);
            date =  itemView.findViewById(R.id.news_date);
            title = itemView.findViewById(R.id.news_title);
            id =  itemView.findViewById(R.id.news_id);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent personalIntent = new Intent(v.getContext(), AdvertActivity.class);
            String id = this.id.getText().toString();
            personalIntent.putExtra("id", id);
            personalIntent.putExtra("type",type);
            v.getContext().startActivity(personalIntent);
        }
    }


}



