package sairam.ontustik.kz.zherkezek.fragment.navigationview;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.fragment.plot.FiledFragment;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.NfAdapter;


public class NewsFragment extends Fragment {
    LinearLayoutManager layoutManager;
    JSONArray jsonArray;
    NfAdapter viewAdapter;
    EmptyRecyclerViewAdapter emptyAdapter;
    RecyclerView recyclerView;
    GetJsonService jsons = new GetJsonService();

    PlotProgressDialog  progressDialog = new PlotProgressDialog();
    ProgressBar progressBar;
    int finishCard=50;
    VerifyConnection verifyConnection = new VerifyConnection();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        if (verifyConnection.isNetworkAvailable(getActivity())){
            initRecyclerView(view);
            new NewsAsyncTask().execute();
        }else{
            verifyConnection.showSnackBar();
        }

        return view;
    }

    private void initRecyclerView(View view) {
        recyclerView = view.findViewById(R.id.recyclerview_fragment_news);
        progressBar=view.findViewById(R.id.loader_news);
        layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1) && newState==RecyclerView.SCROLL_STATE_IDLE) {

                    finishCard=finishCard+50;
                    new NewsAsyncTask().execute(finishCard);


                }

            }


        });

    }



    private class NewsAsyncTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.showLoader(progressBar);
        }

        @Override
        protected Void doInBackground(Integer... params) {

            jsonArray = jsons.getNews(0, finishCard);
            viewAdapter = new NfAdapter(jsonArray,"1");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(jsonArray==null){
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.server_not_found));
                recyclerView.setAdapter(emptyAdapter);
            }
            else if (jsonArray.length()==0){
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.not_news_now));
                recyclerView.setAdapter(emptyAdapter);

            }
            else {
                recyclerView.setAdapter(viewAdapter);
            }
            progressDialog.hideLoader(progressBar);

            recyclerView.getLayoutManager().scrollToPosition(finishCard-50);
        }
    }

}
