package sairam.ontustik.kz.zherkezek;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.widget.FrameLayout;

import sairam.ontustik.kz.zherkezek.fragment.AdvertFragment;

import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP;

public class AdvertActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout frameLayout=(FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_advert,frameLayout);
        AppBarLayout.LayoutParams params =(AppBarLayout.LayoutParams)toolbar.getLayoutParams();
        params.setScrollFlags(SCROLL_FLAG_SCROLL |SCROLL_FLAG_ENTER_ALWAYS |SCROLL_FLAG_SNAP);
        initFragment();


    }
    private void initFragment(){
        String id=getIntent().getStringExtra("id");
        String type=getIntent().getStringExtra("type");

        Fragment contextFragment= new AdvertFragment(id,type);
        fragmentManager.beginTransaction().replace(R.id.content_frame,contextFragment).commit();
    }


}
