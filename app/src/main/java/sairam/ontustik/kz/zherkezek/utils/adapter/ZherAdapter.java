package sairam.ontustik.kz.zherkezek.utils.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.PersonalActivity;
import sairam.ontustik.kz.zherkezek.R;

/**
 * Created by Ilyas on 17.05.2017.
 */

public class ZherAdapter extends RecyclerView.Adapter{
JSONArray list;


    public ZherAdapter(JSONArray list,RecyclerView recyclerView) {
        this.list=list;

    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_fragment,parent,false);
            RecyclerView.ViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            MyViewHolder myViewHolder=(MyViewHolder) holder;
            JSONObject jsonObject=(JSONObject) list.get(position);
            myViewHolder.client_name.setText( jsonObject.getString("fullName"));
            myViewHolder.client_name_date.setText( jsonObject.getString("applyDate"));
            myViewHolder.client_id.setText(jsonObject.getString("id"));
            myViewHolder.ic_queue.setText(position+1+"");
            switch (jsonObject.getInt("status")){
                case 1:
                    myViewHolder.text_status.setText(R.string.tapsirsildi);
                    myViewHolder.img_status.setImageResource(R.drawable.ic_plot_status_wait);
                    break;
                case 2:
                    myViewHolder.text_status.setText(R.string.queue);
                    myViewHolder.img_status.setImageResource(R.drawable.ic_plot_status_queue);
                    break;
                case 3:
                    myViewHolder.text_status.setText(R.string.refuse);
                    myViewHolder.img_status.setImageResource(R.drawable.ic_plot_status_refuse);
                    break;
                case 4:
                    myViewHolder.text_status.setText(R.string.get);
                    myViewHolder.img_status.setImageResource(R.drawable.ic_plot_status_get);
                    break;
                default:
                    myViewHolder.text_status.setText(R.string.unknown);
                    myViewHolder.img_status.setImageResource(R.drawable.ic_unknown);

        }


       }catch (JSONException e){
            e.getMessage();
        }






    }


    @Override
    public int getItemCount() {
        try {
            return list.length();
        }catch (NullPointerException e){
            e.printStackTrace();
            return 0;
        }

    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView client_name,client_name_date,text_status,client_id,ic_queue;
        ImageView img_status;

        public MyViewHolder(View itemView) {
            super(itemView);
            client_name=(TextView) itemView.findViewById(R.id.news_title);
            client_name_date=(TextView) itemView.findViewById(R.id.client_name_date);
            text_status=(TextView) itemView.findViewById(R.id.text_status);
            img_status=(ImageView) itemView.findViewById(R.id.img_status);
            client_id=(TextView)itemView.findViewById(R.id.client_id);
            ic_queue=(TextView) itemView.findViewById(R.id.ic_queue);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent personalIntent= new Intent(v.getContext(), PersonalActivity.class);
            int id=Integer.parseInt(client_id.getText().toString());
            personalIntent.putExtra("id",id);
           v.getContext().startActivity(personalIntent);
        }
    }



}



