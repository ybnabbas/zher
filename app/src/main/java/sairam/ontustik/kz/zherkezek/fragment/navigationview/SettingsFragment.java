package sairam.ontustik.kz.zherkezek.fragment.navigationview;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.google.firebase.messaging.FirebaseMessaging;

import sairam.ontustik.kz.zherkezek.R;


public class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {
    SharedPreferences sp;

    public SettingsFragment() {
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.settings);
        sp = PreferenceManager.getDefaultSharedPreferences(getContext());
        sp.registerOnSharedPreferenceChangeListener(this);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        if (key.equals("news")){
            Boolean value = sharedPreferences.getBoolean(key, false);
            if (value) {
                FirebaseMessaging.getInstance().subscribeToTopic(key);
            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(key);
            }
        }else if (key.equals("auction")){
            Boolean value = sharedPreferences.getBoolean(key, false);
            if (value) {
                FirebaseMessaging.getInstance().subscribeToTopic(key);
            } else {
                FirebaseMessaging.getInstance().unsubscribeFromTopic(key);
            }
        }
    }

}
