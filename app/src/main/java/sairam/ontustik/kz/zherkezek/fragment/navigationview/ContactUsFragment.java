package sairam.ontustik.kz.zherkezek.fragment.navigationview;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.MyPreferences;
import sairam.ontustik.kz.zherkezek.utils.MyResourses;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.Sender;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;


public class ContactUsFragment extends Fragment {

    VerifyConnection verifyConnection = new VerifyConnection();
    PlotProgressDialog progressDialog = new PlotProgressDialog();

    Button sendBtn;
    EditText questionText, phoneText;
    MaterialBetterSpinner village_spinners;
    GetJsonService jsonService = new GetJsonService();
    String iin;
    String selectedVillageName =null;
    MyPreferences myPreferences;
    List<String> villages = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contactus, container, false);
        myPreferences = new MyPreferences(getContext());

        try {
            JSONArray jsonArray = new  JSONArray(myPreferences.getValue("villages"));
            for (int i = 0; i < jsonArray.length(); i++) {
                String villageName = jsonArray.getJSONObject(i).getString("name");
                System.out.println("VillageName is : " + villageName);
                villages.add(villageName);


            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        sendBtn = view.findViewById(R.id.sendBtn);
        questionText = view.findViewById(R.id.questionText);
        phoneText = view.findViewById(R.id.phoneText);

        village_spinners = view.findViewById(R.id.village_spinners);
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_dropdown_item_1line, villages);
        village_spinners.setAdapter(spinnerAdapter);
        System.out.println("MyVillages" + villages);


        iin = myPreferences.getValue("iin");

        if (!verifyConnection.isNetworkAvailable(getActivity())) {
            sendBtn.setVisibility(View.GONE);
            questionText.setVisibility(View.GONE);
            village_spinners.setVisibility(View.GONE);
            phoneText.setVisibility(View.GONE);

            verifyConnection.showSnackBar();
        }
        showHelpWindow();


        sendBtn.setOnClickListener(event -> {
            String question = questionText.getText().toString();
            String phone = phoneText.getText().toString();

            if (question.isEmpty() || question.length() < 15) {
                Toast.makeText(getContext(), getResources().getText(R.string.questionnot_full), Toast.LENGTH_LONG).show();
            } else if (phone.length() < 11) {
                Toast.makeText(getContext(), getResources().getText(R.string.phone_not_full), Toast.LENGTH_LONG).show();
            } else if (selectedVillageName == null) {
                Toast.makeText(getContext(), getResources().getText(R.string.choose_village), Toast.LENGTH_LONG).show();
            } else {



                new SendQuestionAsync().execute(question, phone,selectedVillageName);
            }

        });

        village_spinners.setOnItemClickListener((adapter, v, position, id) -> {

            selectedVillageName = adapter.getItemAtPosition(position).toString();
            System.out.println("Selected  value : " + selectedVillageName);

        });


        return view;

    }


    public void showHelpWindow() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(MyResourses.getString(getContext(), R.string.question_title)).setItems(R.array.question_info, null).setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private class SendQuestionAsync extends AsyncTask<String, Void, Boolean> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(), getString(R.string.sending));
        }


        @Override
        protected Boolean doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            Sender sendQuestion = new Sender();
            try {
                jsonObject.put("iin", iin);

                jsonObject.put("districtId",Integer.parseInt(myPreferences.getValue("districtId")));
                jsonObject.put("title", params[0]);
                jsonObject.put("phone", params[1]);
                jsonObject.put("village", params[2]);
                System.out.println(jsonObject);
                return sendQuestion.sendQuestion(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }


        }

        @Override
        protected void onPostExecute(Boolean result) {
            progressDialog.hideDialog();
            String resultInfo;

            if (result) {
                resultInfo = getResources().getString(R.string.thanks_question);
                questionText.setText("");
            } else if (result == null) {
                resultInfo = getResources().getString(R.string.question_not_send);
            } else {
                resultInfo = getResources().getString(R.string.question_not_send);
            }
            Toast.makeText(getContext(), resultInfo, Toast.LENGTH_LONG).show();
            super.onPostExecute(result);
        }
    }




}
