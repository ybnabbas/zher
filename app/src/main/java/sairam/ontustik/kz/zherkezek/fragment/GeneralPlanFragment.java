package sairam.ontustik.kz.zherkezek.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;

import org.json.JSONException;
import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;


public class GeneralPlanFragment extends Fragment {
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    JSONObject object = new JSONObject();
    VerifyConnection verifyConnection = new VerifyConnection();
    GetJsonService jsons = new GetJsonService();
    PhotoView imageGenPlan;
    TextView isempty;
    int villageId;
    String base64;

    public GeneralPlanFragment(int id) {
        this.villageId = id;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_general_plan,container,false);
        imageGenPlan= (PhotoView) view.findViewById(R.id.imageGenPlan);
        isempty=(TextView) view.findViewById(R.id.isempty);
        imageGenPlan.setMaximumScale(10);
        if (verifyConnection.isNetworkAvailable(getActivity())) {
            new GeneralPlanAsynck().execute(villageId);
        } else {
            verifyConnection.showSnackBar();
        }
        return view;
    }

    private class GeneralPlanAsynck extends AsyncTask<Integer,Void,Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.showDialog(getActivity(),"Жүктелуде...");
        }

        @Override
        protected Void doInBackground(Integer... params) {
            object=jsons.getPassport(params[0]);
            try {
                base64=object.getString("genPlan");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(object==null || base64.isEmpty()){
               imageGenPlan.setVisibility(View.GONE);
                isempty.setVisibility(View.VISIBLE);
            }else{
                imageGenPlan.setVisibility(View.VISIBLE);
                isempty.setVisibility(View.GONE);
                byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                imageGenPlan.setImageBitmap(decodedByte);
            }

            progressDialog.hideDialog();

        }

    }
}
