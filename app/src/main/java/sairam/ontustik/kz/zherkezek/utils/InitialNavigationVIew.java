package sairam.ontustik.kz.zherkezek.utils;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.TextView;

import org.json.JSONObject;

import sairam.ontustik.kz.zherkezek.MainActivity;
import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.fragment.ListVillageFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.AboutUsFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.AuctionFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.ContactUsFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.DeleteFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.MyCardFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.NewsFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.NotificationFragment;
import sairam.ontustik.kz.zherkezek.fragment.navigationview.SettingsFragment;

public class InitialNavigationVIew {
    TextView nav_news, nav_auction, nav_notification;
    FragmentManager fragmentManager;
    Toolbar toolbar;
    public int countNf;
    JSONObject jsonObject = new JSONObject();
    GetJsonService jsons = new GetJsonService();
    Context context;

    public void initilize(AppCompatActivity context, DrawerLayout drawerLayout, NavigationView navigationView, Toolbar toolbar) {
        this.context=context;
      //  new CountAsynctask().execute();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.toolbar=toolbar;
        fragmentManager=context.getSupportFragmentManager();
        ActionBarDrawerToggle toogle = new ActionBarDrawerToggle(context, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.setDrawerListener(toogle);
        toogle.syncState();
        nav_news = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_news));
        nav_auction = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_auction));
        nav_notification = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().findItem(R.id.nav_notification));
        setupDrawerListener(navigationView, drawerLayout);
        initCountDrawer(nav_notification, countNf+"");

    }


     private void  initCountDrawer(TextView textView, String count) {
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTypeface(null, Typeface.BOLD);
        textView.setTextColor(textView.getResources().getColor(R.color.countNavigation));
        textView.setText(count);
    }

    private void setupDrawerListener(NavigationView navigationView, final DrawerLayout drawerLayout) {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                drawerLayout.closeDrawers();
                toolbar.setTitle(item.getTitle());
                selectItem(item);
                return true;
            }
        });
    }

    private void selectItem(MenuItem item) {
        Fragment fragment=null;
        switch (item.getItemId()){
            case R.id.main_page:
                MyPreferences preferences = new MyPreferences(context);

                String districtId=preferences.getValue("districtId");
                fragment= new ListVillageFragment(districtId);
                break;
            case R.id.nav_news:
                fragment = new NewsFragment();
                break;
            case R.id.nav_auction:
                fragment = new AuctionFragment();
                break;
            case R.id.nav_notification:
                fragment = new NotificationFragment();
                break;
            case R.id.nav_settings:
                fragment = new SettingsFragment();
                break;
            case R.id.nav_contactus:
                fragment = new ContactUsFragment();
                break;
            case R.id.nav_about:
                fragment = new AboutUsFragment();
                break;
            case R.id.nav_myCard:
                fragment = new MyCardFragment();
                break;
            case R.id.nav_delete:
                fragment = new DeleteFragment();
                break;
        }
        fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();

    }



}
