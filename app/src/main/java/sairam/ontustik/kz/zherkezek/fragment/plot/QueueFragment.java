package sairam.ontustik.kz.zherkezek.fragment.plot;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONArray;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.utils.GetJsonService;
import sairam.ontustik.kz.zherkezek.utils.PlotProgressDialog;
import sairam.ontustik.kz.zherkezek.utils.adapter.EmptyRecyclerViewAdapter;
import sairam.ontustik.kz.zherkezek.utils.adapter.ZherAdapter;


public class QueueFragment extends Fragment {
    LinearLayoutManager layoutManager;
    ZherAdapter viewAdapter;
    GetJsonService jsons = new GetJsonService();
    JSONArray jsonArray = new JSONArray();
    RecyclerView recyclerView;
    int village;
    PlotProgressDialog progressDialog = new PlotProgressDialog();
    ProgressBar progressBar;
    int finishCard = 50;
    EmptyRecyclerViewAdapter emptyAdapter;

    public QueueFragment() {
    }

    public QueueFragment(int village) {
        this.village = village;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_queue_plot, container, false);
        initRecyclerView(view);
        return view;

    }

    private void initRecyclerView(View view) {

        this.recyclerView = view.findViewById(R.id.recyclerview_queue_plot);
        progressBar = view.findViewById(R.id.loader_queue);
        layoutManager = new LinearLayoutManager(getContext());
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(), layoutManager.getOrientation());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(itemDecoration);

        new WaitAsyncTask().execute(finishCard);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {

                    finishCard = finishCard + 50;
                    new WaitAsyncTask().execute(finishCard);


                }

            }


        });
    }

    private class WaitAsyncTask extends AsyncTask<Integer, Void, Void> {
        @Override
        protected void onPreExecute() {
            progressDialog.showLoader(progressBar);
        }

        @Override
        protected Void doInBackground(Integer... params) {
            jsonArray = jsons.getPlots(0, 0, village, 2, 0, params[0]);
            viewAdapter = new ZherAdapter(jsonArray, recyclerView);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {


            if (jsonArray == null) {
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.server_not_found));
                recyclerView.setAdapter(emptyAdapter);
            } else if (jsonArray.length() == 0) {
                emptyAdapter = new EmptyRecyclerViewAdapter(getResources().getString(R.string.plot_emty));
                recyclerView.setAdapter(emptyAdapter);

            } else {
                recyclerView.setAdapter(viewAdapter);
            }
            progressDialog.hideLoader(progressBar);

            recyclerView.getLayoutManager().scrollToPosition(finishCard-50);
        }


    }
}
