package sairam.ontustik.kz.zherkezek.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Ilyas on 04.07.2017.
 */

public class JSONBuilderService {

    HttpURLConnection connection;
    URL url;
    public JSONObject createObject(String address){
        try {
            url =  new URL(address);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            StringBuilder sb = new StringBuilder();

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line=br.readLine())!=null){
                sb.append(line+"\n");
            }
            return new JSONObject(sb.toString());
        }catch (IOException |JSONException e){
            e.printStackTrace();
            return null;
        }

    }
    public JSONArray createArray(String address){
        try {
            url =  new URL(address);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            while ((line=br.readLine())!=null){
                sb.append(line+"\n");
            }
            return new JSONArray(sb.toString());
        }catch (IOException|JSONException e){
            e.printStackTrace();
            return null;
        }
    }
}
