package sairam.ontustik.kz.zherkezek.utils.adapter;

import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import sairam.ontustik.kz.zherkezek.PopupActivity;
import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.VillageActivity;

/**
 * Created by Ilyas on 17.05.2017.
 * Ильяс popumneu ді көрінбейтін қылдың. Керек кезде қосасын
 *
 *
 *
 *
 */

public class MainAdapter extends RecyclerView.Adapter{
JSONArray list;

    public MainAdapter(JSONArray list) {
        this.list=list;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_main,parent,false);
       RecyclerView.ViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        try {
            MyViewHolder myViewHolder=(MyViewHolder) holder;
            myViewHolder.name.setText(list.getJSONObject(position).getString("name"));
            myViewHolder.villageId.setText(list.getJSONObject(position).getString("id"));
            char firstChar=myViewHolder.name.getText().charAt(0);
            myViewHolder.asd.setText(firstChar+"");

            myViewHolder.popup_menu.setOnClickListener(click->{
                PopupMenu popupMenu = new PopupMenu(myViewHolder.popup_menu.getContext(),myViewHolder.popup_menu);
                popupMenu.inflate(R.menu.popup_menu_main);
                popupMenu.setOnMenuItemClickListener(item -> {
                    Intent popupActivity = new Intent(myViewHolder.name.getContext(), PopupActivity.class);
                    popupActivity.putExtra("item",item.getItemId());
                    popupActivity.putExtra("villageId",position+1);
                    popupActivity.putExtra("village",myViewHolder.name.getText());
                    myViewHolder.popup_menu.getContext().startActivity(popupActivity);
                    return false;
                });
                popupMenu.show();
            });
            switch (firstChar){
                case 'А':
                    myViewHolder.asd.setBackground(myViewHolder.name.getResources().getDrawable(R.drawable.bg_main_sary));
                    break;
                case 'Қ':
                    myViewHolder.asd.setBackground(myViewHolder.name.getResources().getDrawable(R.drawable.bg_main_kok));
                    break;
                case 'Ж':
                    myViewHolder.asd.setBackground(myViewHolder.name.getResources().getDrawable(R.drawable.bg_main_qyzyl));
                    break;
                case 'М':
                    myViewHolder.asd.setBackground(myViewHolder.name.getResources().getDrawable(R.drawable.bg_main_m));
                    break;
                case 'К':
                    myViewHolder.asd.setBackground(myViewHolder.name.getResources().getDrawable(R.drawable.bg_main_zhasyl));
                    break;
                default:
                    myViewHolder.asd.setBackground(myViewHolder.name.getResources().getDrawable(R.drawable.bg_main_sary));
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }





    }


    @Override
    public int getItemCount() {
        try {
            return list.length();
        }catch (Exception e){
            e.printStackTrace();
            return 0;
        }

    }



    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name;
        TextView asd,villageId;
        ImageView popup_menu;
        public MyViewHolder(View itemView) {
            super(itemView);
            name= itemView.findViewById(R.id.name);
            asd= itemView.findViewById(R.id.asd);
            popup_menu=itemView.findViewById(R.id.popup_menu);
            villageId=itemView.findViewById(R.id.villageId);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View v) {
            Intent listPlot=new Intent(v.getContext(), VillageActivity.class);
            listPlot.putExtra("name", name.getText());
            System.out.println("villageId.getText() :" +villageId.getText());
            listPlot.putExtra("itemId",Integer.parseInt(villageId.getText().toString()));
            v.getContext().startActivity(listPlot);

        }
    }

}



