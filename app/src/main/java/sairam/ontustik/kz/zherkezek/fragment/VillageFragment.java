package sairam.ontustik.kz.zherkezek.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sairam.ontustik.kz.zherkezek.R;
import sairam.ontustik.kz.zherkezek.fragment.plot.FiledFragment;
import sairam.ontustik.kz.zherkezek.fragment.plot.GetFragment;
import sairam.ontustik.kz.zherkezek.fragment.plot.QueueFragment;
import sairam.ontustik.kz.zherkezek.fragment.plot.RefuseFragment;
import sairam.ontustik.kz.zherkezek.utils.VerifyConnection;
import sairam.ontustik.kz.zherkezek.utils.adapter.ViewPagerAdapter;


public class VillageFragment extends Fragment {
    private static int ACTIVITY_LIST_PLOT = R.layout.fragment_village;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    VerifyConnection verifyConnection = new VerifyConnection();
    int village;

    public VillageFragment() {
    }

    public VillageFragment(int village) {
        this.village = village;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(ACTIVITY_LIST_PLOT, container, false);
        if (verifyConnection.isNetworkAvailable(getActivity())) {
            initTab(view);
        } else {
            verifyConnection.showSnackBar();
        }
        return view;

    }



    private void initTab(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager_list_plot);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tablayout_list_plot);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(1).select();
// tabLayout.getTabAt(0).setIcon(R.drawable.ic_tab_filed);
// tabLayout.getTabAt(1).setIcon(R.drawable.ic_tab_queue);
// tabLayout.getTabAt(2).setIcon(R.drawable.ic_tab_get);
// tabLayout.getTabAt(3).setIcon(R.drawable.ic_tab_refuse);

    }


  private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        viewPagerAdapter.addFragment(new FiledFragment(village), getResources().getString(R.string.filed));
        viewPagerAdapter.addFragment(new QueueFragment(village), getResources().getString(R.string.queue));
        viewPagerAdapter.addFragment(new GetFragment(village), getResources().getString(R.string.get));
        viewPagerAdapter.addFragment(new RefuseFragment(village), getResources().getString(R.string.refuse));
        viewPager.setAdapter(viewPagerAdapter);

    }

}
