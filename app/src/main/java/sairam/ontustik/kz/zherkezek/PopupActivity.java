package sairam.ontustik.kz.zherkezek;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.FrameLayout;

import sairam.ontustik.kz.zherkezek.fragment.DetDesProFragment;
import sairam.ontustik.kz.zherkezek.fragment.GeneralPlanFragment;
import sairam.ontustik.kz.zherkezek.fragment.ListSubVillagesFragment;
import sairam.ontustik.kz.zherkezek.fragment.PassportFragment;

import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL;
import static android.support.design.widget.AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP;

public class PopupActivity extends BaseActivity {
    int subvillageId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrameLayout frameLayout=(FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(R.layout.activity_personal,frameLayout);
        AppBarLayout.LayoutParams params =(AppBarLayout.LayoutParams)toolbar.getLayoutParams();
        params.setScrollFlags(SCROLL_FLAG_SCROLL |SCROLL_FLAG_ENTER_ALWAYS |SCROLL_FLAG_SNAP);

        int item = getIntent().getIntExtra("item", 1);
        int villageId=getIntent().getIntExtra("villageId",1);
        if (item==3){
            subvillageId=Integer.parseInt(getIntent().getStringExtra("subvillage_id"));
            toolbar.setTitle(getIntent().getStringExtra("village"));
        }
        toolbar.setTitle(getIntent().getStringExtra("village"));
        initFragment(item,villageId);

    }
private void initFragment(int item, int villageId){
    switch (item){
        case R.id.passport:
            Fragment passportFragment= new PassportFragment(villageId);
            fragmentManager.beginTransaction().replace(R.id.content_frame,passportFragment).commit();
            break;
        case R.id.general_plan:
            Fragment generalPlanFragment= new GeneralPlanFragment(villageId);
            fragmentManager.beginTransaction().replace(R.id.content_frame,generalPlanFragment).commit();
            break;
        case R.id.list_pdp:
            Fragment listSubVillages= new ListSubVillagesFragment(villageId);
            fragmentManager.beginTransaction().replace(R.id.content_frame,listSubVillages).commit();
            break;
        case 3:
            Fragment subVillagePdp= new DetDesProFragment(subvillageId);
            Log.i("zher",subvillageId+"");
            fragmentManager.beginTransaction().replace(R.id.content_frame,subVillagePdp).commit();
            break;


    }
}

}
